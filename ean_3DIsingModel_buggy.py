# -*- coding: utf-8 -*-

"""
/*=============================================================================
 |   Assignment:  Ising Model
 |
 |       Author:  Eanna Doyle, 15325737
 |
 |     Language:  Python, tested in Spyder
 |                     
 |       Module:  Computational Methods/Computer Simulation PY3CO1
 |                      
 |   Instructor:  Dr. Thomas Archer, Trinity College Dublin
 |     Due Date:  15th of January 2018 (extended to 29th of January)
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Build working 2-D Ising Model
 |
 |        Input:  Temperature, External Magnetic Field, Lattice Size.
 |
 |       Output:  2-D Lattice of containing spin states
 |
 |    Algorithm:  Metropolis 
 |
 |   Required Features:  Numpy, MatplotLib
 |
 |   Known Bugs: When using pause NameError: name 'time' is not defined
 |    
 |
 *===========================================================================*/
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# INITIAL CONDITIONS
lattice_side = 10
n = lattice_side
lattice_size = (n, n, n) #dimensions of lattice 3D
moment = 1 # Magnetic moment
T = 0.001 # Temperature (in eV)
interaction = 1 # Interaction (ferromagnetic if positive, antiferromagnetic if negative)
H = np.full(lattice_size, 0) # External magnetic field


spins = np.zeros((n,n,n), dtype = int)
for x in range(n):
    for y in range(n):
        for z in range(n):
            spins[x,y,z] = choice([1,-1])

#Configuring intial system with randomnly assigned spins for each point in lattice
#spins = np.random.choice([-1, 1], size=lattice_size)

#Statistical Mechanics - configuring probability of state

KB = 8.6173303e-5 #eV per Kelvin

Beta = 1/(KB*T)

# Function to return probability of flipping spin
#inputs:Ex, Ey - Values of Energy

def Pflip(Ex, Ey, T):
  dE = (Ey - Ex)  
  return np.exp(-Beta*(dE))

#Function to calculate energies from definition of Hamiltonian for Ising  model
#np.roll adds elements that exceed the last position to first position,
#this simulates the single spin flip dynamics
  
def Ising_Hamiltonian(spins):
  return -np.sum(
    interaction * spins * np.roll(spins, 1, axis=0) +
    interaction * spins * np.roll(spins, -1, axis=0) +
    interaction * spins * np.roll(spins, 1, axis=1) +
    interaction * spins * np.roll(spins, -1, axis=1) +
    interaction * spins * np.roll(spins, 1, axis=2) +
    interaction * spins * np.roll(spins, -1, axis=2) 
  )/2 - moment * np.sum(H * spins)


#flips spin of random tuple in spin lattice
def flip_spin(spins, T):
  spins_flipped = np.copy(spins)
  i = np.random.randint(spins.shape[0]) #returns x dimension
  j = np.random.randint(spins.shape[1]) #returns y dimension
  k = np.random.randint(spins.shape[2]) #returns z dimension
  spins_flipped[i, j, k] *= -1 #reverses the spin of tuple
  
  energy_pre_flip = Ising_Hamiltonian(spins)
  energy_post_flip = Ising_Hamiltonian(spins_flipped)
  if Pflip(energy_pre_flip, energy_post_flip, T) > np.random.random(): 
    return spins_flipped
  else:
    return spins

#order parameter, measure disorder of system
#order_parameter=((number of spins up-number of spins down)/number of spins)
def measure_order(spins):
    n_spin_up = np.count_nonzero(spins == 1)
    n_spin_down = np.count_nonzero(spins == -1)
    n_spins = (len(spins))**3
    order_parameter = abs((n_spin_up-n_spin_down)/n_spins)
    return order_parameter
    

#plot_of_ising = plt.imshow(spins, cmap='cool', vmin=-1, vmax=1, interpolation='spline36')
#while loop over random spin states of lattice until equilibrium is maintained
t = 0
order = measure_order(spins)

while True:
  if t % 10 == 0:
    fig1 = plt.figure()
    axis =  fig1.add_subplot(111, projection = '3d')
    pos = np.where(spins==1)
    axis.scatter(pos[0],pos[1],pos[2], c='red')
    pos1 = np.where(spins==-1)
    axis.scatter(pos1[0],pos1[1],pos1[2], c='blue')
    plt.show()

    spins = flip_spin(spins, T)
    x = Ising_Hamiltonian(spins)
    order = measure_order(spins)
    plt.pause(.001)
    t += 1
    print('Energy is = ', x,'time =', t,'array =', spins,'order =',order)
  
    if order == 1:
        fig2 = plt.figure()
        axis =  fig1.add_subplot(111, projection = '3d')
        pos3 = np.where(spins==1)
        axis.scatter(pos3[0],pos3[1],pos3[2], c='red')
        pos2 = np.where(spins==-1)
        axis.scatter(pos2[0],pos2[1],pos2[2], c='blue')
        plt.show()

        print('System has reached Equilibrium after', t, 'seconds')
        break
      

"""
fig1 = plt.figure()
axis =  fig1.add_subplot(111, projection = '3d')
pos = np.where(spins==1)
axis.scatter(pos[0],pos[1],pos[2], c='red')
pos1 = np.where(spins==-1)
axis.scatter(pos1[0],pos1[1],pos1[2], c='blue')
plt.show()
"""
