"""
/*=============================================================================
 |   Assignment:  Ising Model
 |
 |       Author:  Eanna Doyle, 15325737
 |
 |     Language:  Python 3.6, tested in Spyder
 |                     
 |       Module:  Computational Methods/Computer Simulation PY3CO1
 |                      
 |   Instructor:  Dr. Thomas Archer, Trinity College Dublin
 |     Due Date:  15th of January 2018 (extended to 29th of January)
 |
 +-----------------------------------------------------------------------------
 |
 |  Description:  Build working 2-D Ising Model
 |
 |        Input:  Temperature, External Magnetic Field, Lattice Size.
 |
 |       Output:  2-D Lattice of containing spin states
 |
 |    Algorithm:  Metropolis 
 |
 |   Required Features:  Numpy, MatplotLib
 |
 |   Known Bugs: When using pause NameError: name 'time' is not defined
 |    
 |
 *===========================================================================*/
"""
import numpy as np
import matplotlib.pyplot as plt


# INITIAL CONDITIONS
lattice_side = 15
n = lattice_side
lattice_size = (1, n) #dimensions of lattice
moment = 1 # Magnetic moment
T = 0.9 # Temperature (in eV)
interaction = 1 # Interaction (ferromagnetic if positive, antiferromagnetic if negative)
H = np.full(lattice_size, 0) # External magnetic field


#Configuring intial system with randomnly assigned spins for each point in lattice
spins = np.random.choice([-1, 1], size=lattice_size)

#Statistical Mechanics - configuring probability of state

KB = 8.6173303e-5 #eV per Kelvin

Beta = 1/(KB*T)

# Function to return probability of flipping spin
#inputs:Ex, Ey - Values of Energy

def Pflip(Ex, Ey, T):
  dE = (Ey - Ex)  
  return np.exp(-Beta*(dE))

#Function to calculate energies from definition of Hamiltonian for Ising  model
#np.roll adds elements that exceed the last position to first position,
#this simulates the single spin flip dynamics
  
def Ising_Hamiltonian(spins):
  return -np.sum(
    interaction * spins * np.roll(spins, 1, axis=0) +
    interaction * spins * np.roll(spins, -1, axis=0) +
    interaction * spins * np.roll(spins, 1, axis=1) +
    interaction * spins * np.roll(spins, -1, axis=1)
  )/2 - moment * np.sum(H * spins)


#flips spin of random tuple in spin lattice
def flip_spin(spins, T):
  spins_flipped = np.copy(spins)
  i = np.random.randint(spins.shape[0]) #returns n dimension
  j = np.random.randint(spins.shape[1]) #returns m dimension
  spins_flipped[i, j] *= -1 #reverses the spin of tuple
  
  energy_pre_flip = Ising_Hamiltonian(spins)
  energy_post_flip = Ising_Hamiltonian(spins_flipped)
  if Pflip(energy_pre_flip, energy_post_flip, T) > np.random.random(): 
    return spins_flipped
  else:
    return spins

#order parameter, measure disorder of system
#order_parameter=((number of spins up-number of spins down)/number of spins)
def measure_order(spins):
    n_spin_up = np.count_nonzero(spins == 1)
    n_spin_down = np.count_nonzero(spins == -1)
    n_spins = np.count_nonzero(spins)
    order_parameter = abs((n_spin_up-n_spin_down)/n_spins)
    return order_parameter


plot_of_ising = plt.imshow(spins, cmap='coolwarm', vmin=-1, vmax=1, interpolation='bilinear')

#while loop over random spin states of lattice until equilibrium is maintained
order_params=[] #creates an empty list to be appended
t = 0
order = measure_order(spins)
while True:
  if t % 10 == 0:
    plot_of_ising.set_data(spins)
    plt.xlabel('Time = '+str(t)+' cs, Order = '+str(order))
    plt.title('Lattice size   =   '+str(n))
    figsize=(8, 8)
    plt.draw()
  spins = flip_spin(spins, T)
  x = Ising_Hamiltonian(spins)
  order = measure_order(spins)
  order_params.append(order)
  plt.pause(0.01) #centisecond
  t += 1
  
  plt.close
  if t > 1500:
      order_sample = order_params[-1000:] #gives list containing last 1000 order measurements
      plt.close
  
      if len(set(order_sample)) == 1: #tests to if all elements in list are the same              
          plt.text((n-1)/2, 0.5, "Metastable Equilibrium is being maintained", size=15,
                     ha="center", va="center",
                     bbox=dict(boxstyle="round",
                               ec=(1., 0.5, 0.5),
                               fc=(1., 0.8, 0.8),
                               )
                     )
            
          plt.draw()
          plt.show()
          print('Metastable Equilibrium is being maintained after', t, ' cs')
          plt.pause(10)
          plt.close('all')
          break
      
  
 
  if order == 1:
      plot_of_ising.set_data(spins)
      plt.xlabel('Time = '+str(t)+' cs, Order = '+str(order))
      plt.title('Lattice size   =   '+str(n)+'x'+str(n))
      plt.text((n-1)/2,3,"System has reached Homogeneous Equilibrium after "+str(t/100)+' sec', size=15,
                     ha="center", va="center",
                     bbox=dict(boxstyle="round",
                               ec=(1., 0.5, 0.5),
                               fc=(1., 0.5, 0.5),
                               )
                     )
            
      plt.draw()
      plt.show()
      print('System has reached Homogeneous Equilibrium after', t, ' cs')
      plt.pause(10)
      plt.close('all')
      break
  
